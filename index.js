import { input } from "./input.js";

const getObjValue = key => obj => obj[key];

const findKeyRecursevly = (currentObj, [key, ...rest]) => {

}

const readStyles = (input) => {
    // const root = {}
    // Object.keys(input).map(first => {
    //     Object.keys(input[first]).map(second => {
    //         let key = `${first}-${second}`;
    //         if (typeof input[first][second] === "object") {
    //             Object.keys(input[first][second]).map(third => {
    //                 key = `${first}-${second}-${third}`;
    //                 Object.assign(root, { [key]: input[first][second][third] })
    //             })
    //         } else {
    //             Object.assign(root, { [key]: input[first][second] })
    //         }

    //     });
    // })
    // return root
    [0, 1, 2, 3, 4].reduce(function(previousValue, currentValue, index, array) {
        console.log(index)
        return previousValue + currentValue;
      });
    const res = input => Object.keys(input).reduce((acc, a, idx, arr) => {
        console.log(idx)
        if(idx === 1){
            console.log(
                input[acc]
            )
            return {
                [acc]: input[acc]
            }
        }
        console.log(acc)
        if(typeof(input[a]) === "object"){
            console.log(input[a])
            res(input[a]);
        }        
        
        return {...acc, [a]: input[a]}
    })
    return res(input)
}

const createStylesheets = (root) => {
    console.log(root)
    const css = Object.keys(root).map(key => (`
    --${key}: ${root[key]};
    `)).join(``)
    return `
        :root {
            ${css || ""}
        }
    `
}

const cssStr = createStylesheets(readStyles(input));
console.log(cssStr)
/*

        :root {
            
    --colors-black: #000000;
    
    --colors-white: #ffffff;
    
    --colors-grey-10: #737373;
    
    --space-1: 4px;
    
    --space-2: 8px;
    
    --space-3: 12px;
    
    --space-4: 16px;
    
    --space-5: 20px;
    
        }
    
*/