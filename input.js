export const input = {
  colors: {
      black: "#000000",
      white: "#ffffff",
      grey: {
          10 : "#737373"
      }
  }, 
  space: {
      1 : "4px",
      2 : "8px",
      3 : "12px",
      4 : "16px",
      5 : "20px",
  }
};

/* output should be like 

    :root {
        --black: #00000;
        --white: #fffff;
        --grey-10: #737373;
        --space-1: 4px;
        --space-2: 8px;
        --space-3: 12px;
        --space-4: 16px;
        --space-5: 20px;
    }

    .text-color-black {
        color: var(--black);
    }

    .text-color-white {
        color: var(--white);
    }

    .text-color-grey-10 {
        color: var(--grey-10);
    }

    .bg-color-black {
        background-color: var(--black);
    }

    .bg-color-white {
        background-color: var(--white);
    }

    .bg-color-grey-10 {
        background-color: var(--grey-10);
    }
*/